// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

/**
 * @description Function that reverse a given string
 * @param Input field value - text
 */
function reverse(){
	//Get the inputField Value
	var string = $.inputField.value;
	//initialize the reverse String var
	var reverseString = '';	
	//Iterate the string backwards
	for(var i = string.length - 1; i >= 0; i--){
		//Concatenate the chars of the string in the reverseString
		reverseString += string[i];
	}
	//print the reversed String
	Ti.API.log(reverseString);
	$.answerLabel.text = reverseString;
}
/**
 * @description Close Window
 */
function closeWindow(){
	$.test10.close();
}
//Destroy the object and clear the event listener from memory
$.cleanup = function cleanup() {
  	$.destroy();
 	$.off();
};
$.test10.addEventListener('close', $.cleanup);