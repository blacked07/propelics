// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

/**
 * @description Function that compress a string based on repeated characters
 * e.g "aabcccccaaa" would become "a2b1c5a3"
 */
function compress(){
	//initialize the compression string and the count Character
	var countCharacter = 0;
	var compression = '';
	//initialize the last character variable
	var lastCharacter = '';
	//Get the inputField value
	var text = $.inputField.value;
	for(var i in text){
		//Check if the actualCharacter is different with the las Character
		if(text[i] != lastCharacter){
			//If we have some characters counted, compress them.
			if(countCharacter > 0){
				//add the compression to the string as <character><count>
				compression += lastCharacter+countCharacter;
			}
			//Assign the actualCharacter as the last Character
			lastCharacter = text[i];
			//Count this last Character
			countCharacter = 1;
		}else{
			//Increment the count Character
			countCharacter++;
		}	
	}
	//Add the last Character count because of the termination of the for
	compression += lastCharacter+countCharacter;
	//print the compressed string
	Ti.API.log(compression);
	$.answerLabel.text = compression;
}
/**
 * @description Close Window
 */
function closeWindow(){
	$.test8.close();
}
//Destroy the object and clear the event listener from memory
$.cleanup = function cleanup() {
  	$.destroy();
 	$.off();
};
$.test8.addEventListener('close', $.cleanup);