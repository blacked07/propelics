// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var vowels = ['a','e','i','o','u','A','E','I','O','U'];
/**
 * @description Calculate the number o vowels based on an string that the user must write
 */
function calculateVowels(){
	//Initialize the vowels counter and the answer Label
	var totalVowels = 0;
	//Get the inputfield value
	var string = $.inputField.value;
	//Check if the string is not empty
	if(string.length > 0){
		//Calculate the vowels
		for(var i in string){
			//Check the character of the string in the vowels array
			if(vowels.indexOf(string[i]) != -1){
				//Increment the vowels counter
				totalVowels++;
			}
		}
		//Output the vowels counter
		$.answerLabel.text = 'I count '+totalVowels+' vowels in total.';
	}else{
		//ERROR: Empty string
		alert("You must write something!");
		$.answerLabel.text = 'I need at least 1 character to continue.';
	}
}
/**
 * @description Close Window
 */
function closeWindow(){
	$.test1.close();
}
//Destroy the object and clear the event listener from memory
$.cleanup = function cleanup() {
  	$.destroy();
 	$.off();
};
$.test1.addEventListener('close', $.cleanup);
