// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
//Ascending variable
var ascending = 1;
//Sort By variable
var sortBy = 'title';
//Movies Array
var movies = [
	{title:'Matrix',starring:['Keanu Reeves','Laurence Fishburne','Carrie-Anne Moss'],isFavorite:true,score:8.7},
	{title:'The Revenant',starring:['Leonardo DiCaprio','Tom Hardy','Will Poulter'],isFavorite:false,score:8.1},
	{title:'Django Unchained',starring:['Jamie Foxx','Chistoph Waltz','Leonardo DiCaprio'],isFavorite:true,score:8.5},
	{title:'Lord Of the Rings',starring:['Elijah Wood','Ian McKellen','Orlando Bloom'],isFavorite:false,score:8.8},
	{title:'The Dark Knight',starring:['Christian Bale','Heath Ledger','Aaron Eckhart'],isFavorite:true,score:9.0},
	{title:'Inception',starring:['Leonado DiCaprio','Joseph Gordon-Levitt', 'Ellen Page'],isFavorite:true,score:8.8},
	{title:'Gladiator',starring:['Russel Crowe','Joaquin Phoenix', 'Connie Nielsen'],isFavorite:true,score:8.5},
	{title:'Saving Private Ryan',starring:['Tom Hanks','Matt Damon', 'Tom Sizemore'],isFavorite:false,score:8.6},
	{title:'Batman vs Superman',starring:['Ben Affleck','Henry Cavill', 'Amy Adams'],isFavorite:false,score:7.1},
	{title:'Deadpool',starring:['Ryan Reynolds','Morena Baccarin', 'T.J. Miller'],isFavorite:true,score:8.2},
];
/**
 * @description Se the Ascending value and activate buttons
 */
function setAscending(){
	//Mark the other button to unselected
	$.ascending.color = 'white';
	$.descending.color = 'white';
	//mark the button with red color to indicate selected
	this.color = 'red';
	//Change the global value or ascending
	ascending = (this.id == 'ascending'?1:-1);
	sortMovies();	
}

/**
 * @description Set the sortBy value and activate buttons
 */
function setSortBy(){
	sortBy = this.id;
	//set all the buttons unselected
	$.title.color = 'white';
	$.score.color = 'white';
	$.isFavorite.color = 'white';
	//mark the button with red color to indicate selected
	this.color = 'red';
	sortMovies();
}
/**
 * @description Sort Movies function calling to the movies.sort method
 */
function sortMovies(){
	//Create the tableData Array
	var tableData = [];
	//Apply the sort method to the data structure
	movies.sort(compare(sortBy,ascending));
	//Create the rows
	for(var i in movies){
		//Create a Row
		var row = Ti.UI.createTableViewRow({layout:'vertical',height:Ti.UI.SIZE});
		//Add some labels with the attributes of the movies
		row.add(Ti.UI.createLabel({text:movies[i].title, left:'5%',font:{fontSize:22,fontWeight:'bold'}}));
		row.add(Ti.UI.createLabel({text:'Staring: '+movies[i].starring.toString(), left:'5%',top:'4%',font:{fontSize:14}}));
		row.add(Ti.UI.createLabel({text:'Has a score of '+movies[i].score, left:'5%'}));
		//Push the row to the array
		tableData.push(row);
	}
	//set the data to the TableView
	$.movies.setData(tableData);
}
/**
 * @description Compare Function that compare attributes and return sort order
 * @param string attribute - The attribute to be evaluated at the sort function
 * @param integer sortOrder - Determinates if ascending or descending (1 , -1)
 * @return function - The function needed by the sort method
 */
function compare(attribute,sortOrder){
	return function (a,b) {
        var result = (a[attribute] < b[attribute]) ? -1 : (a[attribute] > b[attribute]) ? 1 : 0;
        return result * sortOrder;
   };
}

$.title.fireEvent('click');
/**
 * @description Close Window
 */
function closeWindow(){
	$.test4.close();
}
//Destroy the object and clear the event listener from memory
$.cleanup = function cleanup() {
  	$.destroy();
 	$.off();
};
$.test4.addEventListener('close', $.cleanup);