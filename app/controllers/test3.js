// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var operators_array = ["+","-",'*','/'];
// Object with all the avaible operations of the calculator
var operations = {
	'+':function(x,y){return x + y;},
	'-':function(x,y){return x - y;},
	'*':function(x,y){return x * y;},
	'/':function(x,y){return x / y;}
};
/**
 * @description Calculate the equation given by the user
 */
function calculateEquiation(){
	var n = 0;
	var operators = [];
	var operation = $.inputField.value;
	for(var i in operation){
		//Iterate to find the operators
		if(n = operators_array.indexOf(operation[i]) != -1){//The character is an operator
			//Add the operator to the array
			operators.push(operation[i]);
			//Replace the operator with a comma
			operation = operation.replace(operation[i],',');
		}
	}
	//Validate the operation numbers
	var result = 0;
	var operands = operation.split(',');
	for(var i in operands){
		//Verify the operands if is NaN or empty send an alert
		if(isNaN(operands[i]) || operands[i] == ''){
			alert("Your operation contains a NaN");
			//Break the iteration
			return false;	
		}else if(i == 0){
			//Init the result
			result = operands[i].indexOf('.') != -1?parseFloat(operands[i]):parseInt(operands[i]);			
		}else if(i > 0){
			//Calculate the result for the rest of the operands
			var x = operands[i].indexOf('.') != -1?parseFloat(operands[i]):parseInt(operands[i]);
			Ti.API.log(result+' '+operators[i - 1]+' '+x);
			result = operations[operators[i - 1]](result,x);
		}	
	}
	//Print the result	
	$.answerLabel.text = "The Result is "+result;
}
/**
 * @description Close Window
 */
function closeWindow(){
	$.test3.close();
}
//Destroy the object and clear the event listener from memory
$.cleanup = function cleanup() {
  	$.destroy();
 	$.off();
};
$.test3.addEventListener('close', $.cleanup);