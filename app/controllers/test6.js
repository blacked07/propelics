// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/**
 * @description Function that calculate the permutation on a given string by the user
 * this function calculates the permutation recursively so we need to calculate all the posible permutations of B from A until we empty A and get to the leafs of the string.
 * @param a - text is the rest of our string not including the actual string B that is the root of the permutation.
 * @param b - text is the string to be permuted, at the beginning is empty because we hace all the string in A
 * */
function permutation(a,b){
	//Verify that the rest of the string is not empty
	if(a.length > 0 && a != '')	{
		//iterate and call the function recursively
		for(var i = 0; i < a.length; i++){
			//Store the actual Character
			var actualCharacter = a[i];
			//Cut the rest of the string that not include the actual character
			var restString = a.substr(0,i) + a.substr(i+1,a.length-1);
			//Iteretate for the actual character and the rest of the string
			//In the recursive call add the new actual character for the next call
			//the objective is to empty the string on A to get one permutation for each character B iterated at the beginning
			permutation(a.substr(0,i) + a.substr(i+1,a.length-1), b + a[i]);	
		}	
	}else{
		//if the rest of the string is empty, print the permutation on the leaf
		Ti.API.log(b);
	}
}
/**
 * @description Function call by the UI button on the Window
 */
function calculate(){
	//Call the permutation function and initialize the character to empty for the root of the recursive function
	permutation($.inputField.value,'');	
	$.answerLabel.text = 'Check your console log';
}
/**
 * @description Close Window
 */
function closeWindow(){
	$.test6.close();
}
//Destroy the object and clear the event listener from memory
$.cleanup = function cleanup() {
  	$.destroy();
 	$.off();
};
$.test6.addEventListener('close', $.cleanup);