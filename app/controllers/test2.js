// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/**
 * @description Calculate the time difference of two pickers in minutes
 */
function calculateMinutes(){
	//Calculate the time difference and converting into minutes.
	var timeDifference = parseInt(Math.ceil(($.end_picker.value.getTime() - $.start_picker.value.getTime()) / 60000));	
	$.answerLabel.text = 'The difference is '+timeDifference+' '+(timeDifference == 1?'minute':'minutes');
}

/**
 * @description Close Window
 */
function closeWindow(){
	$.test2.close();
}
//Assign a date to the pickers, + 5 minutes to the end picker to avoid the same time
$.start_picker.minDate = new Date();
$.end_picker.minDate = new Date();
$.end_picker.value = new Date($.start_picker.value.getTime() + (5 * 60000));
//Destroy the object and clear the event listener from memory
$.cleanup = function cleanup() {
  	$.destroy();
 	$.off();
};
$.test2.addEventListener('close', $.cleanup);
