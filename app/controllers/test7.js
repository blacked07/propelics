// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var data = ["November", "is", "the", "coolest", "month", "of", "the", "Year"] ;
/**
 * @description Function that prints a list of strings, one per line in a rectangular frame
 * @param data - array The array of strings to be printed
 * @param size - The Size of the last word, is initialized with zero.
 * */
function printDataOnBox(data,size){
	//Initialize the item size
	var itemSize = 0;
	//Take the last item of the array
	var item = data.pop();
	//Verify the length of our words array
	if(data.length == 0){
		//We are out of items - base case.
		//Compares the length of the actual string and the last item extracted from the array
		itemSize = item.length > size?item.length:size;	
		//*************************
		//Print the edge line based on the bigger size and add 3 more stars to fill the frame
		Ti.API.log(Array(itemSize + 3).join('*'));
	}else{
		//Get the item Size recursively, compare the actual item size with the last extacted item from the array
		itemSize = printDataOnBox(data,item.length > size?item.length:size);	
	}
	//Add some spaces to the actual item until match to the itemSize
	while(item.length < itemSize){
		item += ' ';
	}
	//print the item with the frame stars
	Ti.API.log('*'+item+'*');
	//return the itemSize to the upper lever of the recursion
	return itemSize;
}
/**
 * @description Function that fire the printDataOnBox when fired the event open of the Window
 * */
$.test7.addEventListener('open',function(){
	//Calculate the size of the frame by recursion
	var itemSize = printDataOnBox(data,0);
	//Print the last line of stars
	Ti.API.log(Array(itemSize + 3).join('*'));
});
/**
 * @description Close Window
 */
function closeWindow(){
	$.test7.close();
}
//Destroy the object and clear the event listener from memory
$.cleanup = function cleanup() {
  	$.destroy();
 	$.off();
};
$.test7.addEventListener('close', $.cleanup);