// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var m;
var args = $.args;
/**
 * @description Recursive Method for calculate Series
 */
function calculateSeriesRecursive(k){
	if(k == 1)
		return parseFloat(1);
	else if(k > 1){
		return parseFloat(Math.pow(-1,k + 1)/((2 * k) - 1)) + parseFloat(calculateSeriesRecursive(k - 1));
	}	
}

/**
 * @description Iterative method for calculate series
 */
function calculateSeries(k){
	var result = 0.0;
	$.progress.value = 0;
	for(var i = 1; i <= k; i++){
		$.progress.value = Math.ceil((i * 100) / k);
		result = result + parseFloat(Math.pow(-1,i + 1)/((2 * i) - 1));
	}
	$.answerLabel.text = result;
	return result;
}

/**
 * @description call the function that calculates the series
 */
function calculate(){
	//Print the result
	Ti.API.log(calculateSeries(10e5));
	//Ti.API.log(calculateSeriesRecursive(10e3));	
}
/**
 * @description Event listener to do de calculation of the series, is added as an event listener
 * to avoid locking the UI thread.
 */
Ti.App.addEventListener('calculateSeries',calculate);
/**
 * @description Event listener to calculate the series when the window is opened
 */
$.test5.addEventListener('open', function(){
	Ti.App.fireEvent("calculateSeries");	
});
/**
 * @description Close Window
 */
function closeWindow(){
	$.test5.close();
}
//Destroy the object and clear the event listener from memory
$.cleanup = function cleanup() {
	Ti.App.removeEventListener('doLongTask',test);
  	$.destroy();
 	$.off();
};
$.test5.addEventListener('close', $.cleanup);