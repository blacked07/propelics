// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

/**
 * @description Close Window
 */
function closeWindow(){
	$.test9.close();
}
//Destroy the object and clear the event listener from memory
$.cleanup = function cleanup() {
  	$.destroy();
 	$.off();
};
$.test9.addEventListener('close', $.cleanup);