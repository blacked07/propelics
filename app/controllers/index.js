
/**
 * @description Open a window based on the object attribute <actionWindow> that triggers the click
 * @author Edwin Moedano edwinmoedano@gmail.com
 */
function buttonClick(){
	Alloy.createController(this.actionWindow).getView().open();	
}

$.index.open();
